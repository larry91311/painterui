﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PainterUI : MonoBehaviour
{
    //記錄點
    List<Vector3> allPoints;
    //映射用圖片
    Texture2D paintText;
    private void Start() {
        allPoints=new List<Vector3>();
        //TestPointer();
        
    }

    private void Update() {
        if(Input.GetMouseButton(0))
        {
            Vector3 tmpView=Camera.main.ScreenToViewportPoint(Input.mousePosition);
            allPoints.Add(tmpView);
        }


        if(Input.GetMouseButtonUp(0))
        {
            GenerateText();
            allPoints.Clear();
        }
    }




    //GL
    // When added to an object, draws colored rays from the
    // transform position.
    public int lineCount = 100;
    public float radius = 3.0f;

    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    // Will be called after all regular rendering is done
    public void OnRenderObject()
    {
        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);

        //GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        //GL.MultMatrix(transform.localToWorldMatrix);

        // Draw lines
        GL.LoadOrtho();   //改投影方式
        GL.Begin(GL.LINES);
        GL.Color(Color.red);
        for (int i = 1; i < allPoints.Count; i++)
        {
            Vector3 tmpFront=allPoints[i-1];     
            Vector3 tmpBack=allPoints[i];

            GL.Vertex3(tmpFront.x,tmpFront.y,tmpFront.z);    
            GL.Vertex3(tmpBack.x,tmpBack.y,tmpBack.z);


            
        }
        GL.End();
        //GL.PopMatrix();
    }



    //點陣圖
    public void TestPointer()  //嘗試在某個for範圍畫滿點在QUAD上
    {
        paintText=new Texture2D(300,400);
        for(int i=0;i<300;i++)
        {
            for(int j=0;j<20;j++)
            {
                paintText.SetPixel(i,j,Color.red);
            }
        }
        paintText.Apply();
    }

    public void GenerateText()
    {
        paintText=new Texture2D(300,400);
        for(int i=1;i<allPoints.Count;i++)
        {
            // Vector3 tmpPoint=allPoints[i];
            // int xx=(int)(paintText.width*tmpPoint.x);
            // int yy=(int)(paintText.height*tmpPoint.y);
            // paintText.SetPixel(xx,yy,Color.red);

            Vector3 tmpFront=allPoints[i-1];     
            Vector3 tmpBack=allPoints[i];


            //線的方式:加插值Lerp
            float frontX=(paintText.width*tmpFront.x);
            float frontY=(paintText.height*tmpFront.y);

            float backX=(paintText.width*tmpBack.x);
            float backY=(paintText.height*tmpBack.y);

            int tmpCount=50;
            for(int j=0;j<tmpCount;j++)
            {
                int tmpXX=(int)Mathf.Lerp(frontX,backX,j/(float)tmpCount);
                int tmpYY=(int)Mathf.Lerp(frontY,backY,j/(float)tmpCount);
                paintText.SetPixel(tmpXX,tmpYY,Color.red);
            }
        }

        paintText.Apply();
        transform.GetComponent<Renderer>().material.mainTexture=paintText;
    }
}
